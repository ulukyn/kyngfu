import os
import re
import sys
import json
import time
import shutil
import urllib
import zipfile
import appdirs
import platform
import subprocess
from io import BytesIO
from kyss.module import KyssModule

class ValheimMods(KyssModule):
	
	def getUsedZones(self):
		return {"main": 100}
	
	def _getSteamInstallPath(self):
		if platform.system() == "Windows":
			import winreg
			try:
				key = winreg.OpenKeyEx(winreg.HKEY_LOCAL_MACHINE, r"SOFTWARE\Wow6432Node\Valve\Steam")
				value = winreg.QueryValueEx(key, "InstallPath")
				if key:
					winreg.CloseKey(key)
				return str(value[0])
			except Exception as e:
				return ""
		return ""

	def call_SwitchVModState(self, mod, version):
		checked = self.getE("#-vmod-"+mod, "checked")
		if checked:
			self.checkInstalledMod(mod, version, "install")
		else:
			self.checkInstalledMod(mod, version, "uninstall")
	
	def checkInstalledMod(self, mod, version, operation="check"):
		cwd = os.getcwd()
		valheim_path = self.valheim_path+"/BepInEx/plugins/"
		mod_path = self.roaming_path+"/"+mod+"_"+version+"/"
		if mod == "BepInEx":
			valheim_path = self.valheim_path
			os.chdir(mod_path+"BepInExPack_Valheim")
		elif os.path.isdir(mod_path+mod+"/plugins"):
			os.chdir(mod_path+mod+"/plugins")
		elif os.path.isdir(mod_path+"plugins"):
			os.chdir(mod_path+"plugins")
		else:
			os.chdir(mod_path)

		all_installed = True
		for file in os.listdir("."):
			if os.path.isdir(file):
				installed = os.path.isdir(valheim_path+file)
				if operation == "install" and not installed:
					print("install path", file)
					shutil.copytree(file, valheim_path+file)
				elif operation == "uninstall" and installed:
					shutil.rmtree(valheim_path+file)
					print("uninstall path", file)
			if os.path.isfile(file):
				installed = os.path.isfile(valheim_path+file)
				if operation == "install" and not installed:
					shutil.copy(file, valheim_path+file)
				elif operation == "uninstall" and installed:
					os.unlink(valheim_path+file)
			
			if not installed :
				all_installed = False
		
		os.chdir(cwd)
		
		return all_installed
		

	def setup(self):
		# check valheim steam folder
		if platform.system() == "Windows":
			steam_path = self._getSteamInstallPath()
			steamlibfile = self._getSteamInstallPath()+"\\steamapps\\libraryfolders.vdf"
		else:
			steamlibfile = appdirs.user_data_dir("Steam", "", roaming=True)+"/steamapps/libraryfolders.vdf"
		with open(steamlibfile) as f:
			steamlib = f.read().split("\n")
		
		self.valheim_path = ""
		for line in steamlib:
			sline = line.replace("\"", "").split("\t")
			if len(sline) > 4 and sline[2] == "path":
				path = os.path.abspath(sline[4] + "/steamapps/common")
				if os.path.isdir(path):
					for game in os.listdir(path):
						if game == "Valheim":
							self.valheim_path = os.path.abspath(path+"/Valheim/")
		
		self.roaming_path = appdirs.user_data_dir("valheimods", "", roaming=True)
		self.log("Setup VMods")
		try:
			vmods = self.downloadFile("https://kyn.ryzom.com/valheimods/mods.json").decode("utf8")
		except:
			vmods = ""
		vmods = json.loads(vmods)

		options = ""
		
		for name, vmod in vmods.items():
			mod_path = self.roaming_path+"/"+name+"_"+vmod[2]
			if not os.path.isdir(mod_path):
				file = self.downloadFile("https://kyn.ryzom.com/valheimods/"+name+"_"+vmod[2]+".zip")
				filebytes = BytesIO(file)
				myzipfile = zipfile.ZipFile(filebytes)
				myzipfile.extractall(mod_path)
			
			good_version = vmod[3]
			version = vmod[2]
			if name == "BepInEx":
				bepinex_installed = good_version and self.checkInstalledMod(name, vmod[2], "install")
			else:
				checked = ""
				if self.checkInstalledMod(name, vmod[2]):
					checked="checked"
				if not good_version:
					version += " <span style=\"color: pink\">(Mise à jour disponible sur Nexus mods)</span>"
				options += "<input id='valheim_mods-vmod-"+name+"' type='checkbox' "+checked+" onclick='pywebview.api.run(\""+self.id+"\", \"SwitchVModState\", \""+name+"\", \""+vmod[2]+"\")' /> "+name+" <a href='"+vmod[1]+"' target='_blank'>🌐</a> <span style='color: orange'>"+vmod[0]+"</span> <span style='font-size: 0.7em; color: lightgreen'>"+version+"</span><br />"
		
		infos = self.downloadFile("https://kyn.ryzom.com/valheimods/infos.json").decode("utf8")
		infos = json.loads(infos)
		if infos["local_version"] == infos["remote_version"]:
			uptodate = "<span style='color: lightgreen'>👍 A jour</span>"
		else:
			uptodate = "<span style='color: pink'>😱 A besoin d'une mise à jour</span>"

		html = self.getTemplateV2("main", {
			"options": options,
			"valheim_path": self.valheim_path.replace("\\", "\\\\"),
			"valheim_path2": self.valheim_path.replace("\\", "\\\\").replace("\\", "\\\\"),
			"bepinex_installed?" : bepinex_installed,
			"status": uptodate,
			"server_name": infos["name"],
			})

		self.setZone("main", html)
		KyssModule.setup(self)

