import os
import re
import sys
import json
import time
import shutil
import urllib
import zipfile
import appdirs
import platform
import subprocess
from io import BytesIO
from kyss.module import KyssModule

class Servers(KyssModule):
	
	def getUsedZones(self):
		return {"main": 100}
	
	def _getSteamInstallPath(self):
		if platform.system() == "Windows":
			import winreg
			try:
				key = winreg.OpenKeyEx(winreg.HKEY_LOCAL_MACHINE, r"SOFTWARE\Wow6432Node\Valve\Steam")
				value = winreg.QueryValueEx(key, "InstallPath")
				if key:
					winreg.CloseKey(key)
				return str(value[0])
			except Exception as e:
				return ""
		return ""
	
	def call_StartServer(self, server):
		self.setZone("popup-content", self.getTemplateV2("start_server", {"server": server}))
		self.showE("popup")

	def call_DoStartServerEnshrouded(self):
		self.downloadFile("https://kyn.ryzom.com/enshrouded/status.php?start={}".format(urllib.parse.quote(self.getE("password")["value"])))
		self.hideE("popup")
		
	def call_StartEnshrouded(self):
		steam_path = self._getSteamInstallPath()
		if steam_path:
			subprocess.Popen([steam_path+"\\steam.exe", "steam://rungameid/1203620"])
		else:
			subprocess.Popen(["steam", "steam://rungameid/1203620"])

	def call_DoStartServerValheim(self):
		self.downloadFile("https://kyn.ryzom.com/valheimods/status.php?start={}".format(urllib.parse.quote(self.getE("password")["value"])))
		self.hideE("popup")
		
	def call_StartValheim(self):
		steam_path = self._getSteamInstallPath()
		if steam_path:
			subprocess.Popen([steam_path+"\\steam.exe", "steam://rungameid/892970"])
		else:
			subprocess.Popen(["steam", "steam://rungameid/892970"])
			
			
	def setup(self):
		html = self.getTemplateV2("main", {})
		self.setZone("main", html)
		KyssModule.setup(self)
	
	def start(self):
		while not self.StopAsked:
			status =  self.downloadFile("https://kyn.ryzom.com/enshrouded/status.php").decode("utf8").strip().split("\n")
			if status[0] == "active":
				infos = "<span style='color: lightgreen'>✅ En fonctionnement <input type='button' value='Redémarrer le serveur' onclick='pywebview.api.run(\""+self.id+"\", \"StartServer\", \"Enshrouded\")'/></span>"
			elif status[0] == "activating":
				infos = "<span style='color: orange'>♻️ En cours de lancement</span>"
			else:
				infos = "<span style='color: pink'>⛔️ Ne tourne pas</span> <input type='button' value='Démarrer le serveur' onclick='pywebview.api.run(\""+self.id+"\", \"StartServer\", \"Enshrouded\")'/>"
			
			if status[1] == "latest":
				uptodate = "<span style='color: lightgreen'>👍 A jour</span>"
			else:
				uptodate = "<span style='color: pink'>😱 A besoin d'une mise à jour</span>"
			
			if status[2] == "0":
				players = "<span style='color: gray'>⭕️ Personne ne joue</span>"
			elif status[2] == "1":
				players = "<span style='color: #A9D5D5'>🤹 Quelqu'un s'amuse tout seul</span>"
			else:
				players = "<span style='color: cyan'>🤼 "+status[2]+" joueurs s'éclatent ensemble</span>"
			
			self.setE("enshrouded-status", uptodate+" "+infos+"<br />"+players)
			
			status =  self.downloadFile("https://kyn.ryzom.com/valheimods/status.php").decode("utf8").strip().split("\n")
			if status[0] == "active":
				infos = "<span style='color: lightgreen'>✅ En fonctionnement</span>"
			elif status[0] == "activating":
				infos = "<span style='color: orange'>♻️ En cours de lancement</span>"
			else:
				infos = "<span style='color: pink'>⛔️ Ne tourne pas</span> <input type='button' value='Démarrer' onclick='pywebview.api.run(\""+self.id+"\", \"StartServer\", \"Valheim\")'/>"

			if status[1] == "latest":
				uptodate = "<span style='color: lightgreen'>👍 A jour</span>"
			else:
				uptodate = "<span style='color: pink'>😱 A besoin d'une mise à jour</span>"

			self.setE("valheim-status", uptodate+" "+infos)
			time.sleep(1)

