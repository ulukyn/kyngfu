
import os
import re
import sys
import urllib3
import certifi
import platform
import subprocess
import configparser

http = urllib3.PoolManager(cert_reqs="CERT_REQUIRED",  ca_certs=certifi.where())

local_config = configparser.ConfigParser()
local_config.read("setup.cfg")
remote_config = local_config

url = local_config["metadata"]["setup"]
try:
	response = http.request("GET", url, preload_content=False)
except:
	print("Error downloading A {}".format(url))
else:
	if not response.status == 200:
		print("Error Downloading {}".format(url))
	else:
		data = response.read().decode()
		response.release_conn()
		remote_config = configparser.ConfigParser()
		remote_config.read_string(data)

local_version = float(local_config["metadata"]["version"])
remote_version = float(remote_config["metadata"]["version"])

app_name = local_config["metadata"]["name"]
app_name = app_name[0].upper()+app_name[1:]
print(app_name, "version : {} vs {}".format(local_version, remote_version))
if local_version < remote_version:
	with open("setup.cfg", "w") as f:
		f.write(data)
	config = remote_config
else:
	config = local_config

kyss_setup = config["metadata"]["kyss"]
if kyss_setup[:18] == "https://gitlab.com":
	kyss_setup_url = kyss_setup.replace("/archive/", "/raw/").replace(kyss_setup.split("/")[8], "setup.cfg")
elif kyss_setup[:18] == "https://github.com":
	kyss_setup_url = kyss_setup.replace("/archive/refs/tags/", "/raw/").replace(".zip", "/setup.cfg")

remote_version = 0

try:
	response = http.request("GET", kyss_setup_url, preload_content=False)
except:
	print("Error downloading A {}".format(url))
else:
	data = response.read().decode()
	response.release_conn()
	kyss_config = configparser.ConfigParser()
	kyss_config.read_string(data)
	remote_version = float(kyss_config["metadata"]["version"])

call = [sys.executable, "-m", "pip", "list"]
process = subprocess.Popen(call, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
out, err = process.communicate()
modules = out.decode("utf-8").replace("\r", "")
local_version = -1
for line in modules.split("\n")[2:]:
	sline = re.split(r"\s+", line)
	if len(sline) >= 2:
		if sline[0].lower() == "kyss":
			local_version = float(".".join(sline[1].split(".")[:2]))

print("Kyss version : {} vs {}".format(local_version, remote_version))
if local_version < remote_version:
	if platform.system() == "Darwin":
		subprocess.run(["curl", kyss_setup, "--output", "kyss.zip"])
		subprocess.run(["rm", "-rf", "kyss-main"])
		subprocess.run(["unzip", "kyss.zip"])
		os.chdir("kyss-main")
		os.remove("setup.cfg")
		subprocess.run([sys.executable, "setup.py", "install", "--force"])
		os.chdir("..")
		subprocess.run(["rm", "-rf", "kyss-main"])
	else:
		subprocess.run([sys.executable, "-m", "pip", "uninstall", "-y", "kyss"])
		subprocess.run([sys.executable, "-m", "pip", "install", "--upgrade", kyss_setup])


os.chdir("app")
sys.path.append(os.getcwd())

from kyss import kyss
kysspy = kyss.Kyss(config, kyss_config)
kysspy.start()
