import os
import shutil

####
#
# Patch v1.1
#
# Delete site-packages, new version will use modules instead
#

shutil.rmtree("site-packages/")
os.mkdir("site-packages/")
